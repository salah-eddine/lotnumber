var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var sku = new Schema({
    sku: { type: Number, required: true, unique: true },
    date_maj: { type: Date }
});

module.exports = mongoose.model('SkuModel', sku);