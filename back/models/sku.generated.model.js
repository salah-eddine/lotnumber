var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var skuGenerated = new Schema({
    sku: { type: Number, required: true },
    date: { type: Date, required: true },
    lot: { type: String, required: true }
});

module.exports = mongoose.model('SkuGeneratedModel', skuGenerated);