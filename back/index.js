var express = require('express');
var app = new express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var cors = require('cors');
var morgan = require('morgan');
var SkuModel = require('./models/sku.model');
var SkuGeneratedModel = require('./models/sku.generated.model');

var PORT = 8080;

app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));

function ConnectDatabase() {
    const promise = mongoose.connect('mongodb://localhost:27017/lotnumber', { useNewUrlParser: true });

    const db = mongoose.connection;

    db.on('error', () => {
        console.log('Failed to connect to mongoose')
    }).once('open', () => {
        console.log('Connected to mongoose')
    });
}

ConnectDatabase();

app.route('/sku/new').post((req, res) => {
    var newSkus = req.body.skus;
    for (var i = 0; i < newSkus.length; i++) {
        var query = { sku: newSkus[i] },
            update = { date_maj: new Date() },
            options = { upsert: true, new: true, setDefaultsOnInsert: true };
        SkuModel.findOneAndUpdate(query, update, options, function (error, result) {
            if (error) {
                console.log(error);
            }
        });
    }
    return res.status(200).json(newSkus);
});

app.route('/sku/list').get((req, res) => {
    SkuModel.find({})
        .exec()
        .then(function (skus) {
            return res.status(200).json(skus.map(val => val.sku));
        })
        .catch(error => {
            return res.status(500).json({
                error: error
            });
        });
});

app.route('/lot/new').post((req, res) => {
    var skuGeneratedModel = new SkuGeneratedModel({
        sku: req.body.sku,
        date: new Date(),
        lot: req.body.lot
    });
    skuGeneratedModel.save(function (error) {
        if (error) {
            return res.status(500).json({
                error
            });
        }
        res.status(201).json(req.body);
    });
});

app.route('/lot/all').get((req, res) => {
    SkuGeneratedModel.find({})
        .exec()
        .then(function (skus) {
            return res.status(200).json(skus.map(val => val.sku));
        })
        .catch(error => {
            return res.status(500).json({
                error: error
            });
        });
});

app.route('/lot/last/:sku').get((req, res) => {
    var sku = req.params.sku;
    SkuGeneratedModel.find({ sku })
        .exec()
        .then(function (skus) {
            if (skus && skus.length > 0) {
                var lastLot = skus.reduce((a, b) => a.date > b.date ? a : b);
                var response = lastLot.toObject();
                delete response['_id'];
                delete response['__v'];
                return res.status(200).json(res.json(response));
            } else {
                return res.status(200).json({});
            }

        }).catch(error => {
            console.log(error);
            return res.status(500).json({
                error: error
            });
        });
});

app.route('/lot/current/:sku').get((req, res) => {
    var sku = req.params.sku;
    var count = 0;
    var currentDate = new Date();
    var alphabet = ['A', 'B', 'C', 'D', 'E', 'F',
        'G', 'H', 'J', 'K', 'L', 'M', 'N',
        'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    SkuGeneratedModel.find({ sku })
        .exec()
        .then(function (skus) {
            for (var i = 0; i < skus.length; i++) {
                if (skus[i]
                    &&
                    (skus[i].sku == sku)
                    &&
                    (skus[i].date.getMonth() === currentDate.getMonth())
                    &&
                    (skus[i].date.getFullYear() === currentDate.getFullYear())
                ) {
                    count++;
                }
            }
            return res.status(200).json(count <= 9 ? count : alphabet[count - 10]);
        })
        .catch(error => {
            console.log(error);
            return res.status(500).json({
                error: error
            });
        });
});

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})