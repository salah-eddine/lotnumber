import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from '../../../node_modules/rxjs';

@Injectable({
  providedIn: 'root'
})
export class LotService {

  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private http: HttpClient) { }

  getSkuList(): Observable<any> {
    return this.http.get('http://localhost:8080/sku/list');
  }

  saveLotNumber(sku: number, lot: string): Observable<any> {
    return this.http.post<any>('http://localhost:8080/lot/new', { sku, lot }, { headers: this.headers });
  }

  getCurrentLotNumber(sku: number): Observable<any> {
    return this.http.get(`http://localhost:8080/lot/current/${sku}`);
  }

  getLastLotNumber(sku: number): Observable<any> {
    return this.http.get(`http://localhost:8080/lot/last/${sku}`);
  }

  saveNewSku(skus: number[]): Observable<any> {
    return this.http.post<any>('http://localhost:8080/sku/new', { skus }, { headers: this.headers });
  }

}
