import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { LotService } from './services/lot.service';
import * as XLSX from 'xlsx';

type AOA = any[][];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  data: AOA = [];

  _subscription: Subscription;

  lastLot = null;

  myControl = new FormControl();
  options: number[] = [];
  filteredOptions: Observable<number[]>;
  list: string[] = [
    'Idelle',
    'Mary Kay',
    'Jack Black',
    'Winning Slts',
    'Loreal OMA/CTK',
    'Loreal PWY',
    'Laura Mercier'
  ];

  file: any;
  lot_number_start_value = '- - - -';
  lot_number = this.lot_number_start_value;
  date: Date;

  constructor(private lotService: LotService) {
  }

  optionSelected(option) {
    this.loadLastLot(option);
  }

  loadLastLot(sku) {
    this.lotService.getLastLotNumber(sku).subscribe(res => {
      if (res && res.lot) {
        this.lastLot = res;
      } else {
        this.lastLot = null;
      }
    })
  }

  generate() {
    const sku = this.myControl.value;
    if (sku) {
      this.lotService.getCurrentLotNumber(sku).subscribe(result => {
        let lotNumber = 'MY';
        const dt = new Date();
        this.date = dt;
        const year_time = dt.getFullYear();
        if (year_time === 2018) {
          lotNumber = lotNumber + 'R';
        }
        if (year_time === 2019) {
          lotNumber = lotNumber + 'S';
        }
        if (year_time === 2020) {
          lotNumber = lotNumber + 'T';
        }
        lotNumber += result;
        this.lotService.saveLotNumber(sku, lotNumber).subscribe(res => {
          if (res && res.sku == sku) {
            this.lot_number = lotNumber;
            this.loadLastLot(sku);
          }
        });
      });
    }
  }

  ngOnInit() {
    this.loadSkuLust();
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  loadSkuLust() {
    this._subscription = this.lotService.getSkuList().subscribe(result => {
      if (result) {
        this.options = result;
      }
    });
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private _filter(value: string): number[] {
    const filterValue = value;
    if (this.options.length > 0) {
      return this.options.filter(option => option.toString().includes(filterValue));
    } else {
      return [];
    }
  }

  onFileChange(evt) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) { throw new Error('Cannot use multiple files'); }
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  saveNewSku(sku) {
    if (this.data && this.data.length > 0) {
      console.log('multiple sku');
      const skus = this.data.filter(elm => elm && elm.length > 0).map(elm => Number(elm[0]));
      this.lotService.saveNewSku(skus).subscribe(res => {
        console.log(res);
        this.loadSkuLust();
      });
    } else if (sku) {
      console.log('single sku');
      this.lotService.saveNewSku([sku]).subscribe(res => {
        console.log(res);
        this.loadSkuLust();
      });
    }
  }

}


